<?php

namespace ConferenciaCorp\Middleware\Autenticacao;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;

class Required implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        if (empty($request->getAttribute('user'))) {
            throw new \InvalidArgumentException('Token obrigatório', 401);
        }

        return $delegate->process($request);
    }
}
