<?php

namespace ConferenciaCorp\Middleware\Autenticacao;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use ConferenciaCorp\Autenticacao\Autenticacao;

class Valid implements MiddlewareInterface
{
    private $autenticacao;

    public function __construct(Autenticacao $autenticacao)
    {
        $this->autenticacao = $autenticacao;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        $token = '';
        if (!empty($request->getAttribute('token'))) {
            $token = $request->getAttribute('token');
        } elseif (
            preg_match('/^Bearer\s(?<token>.+)/', $request->getHeaderLine('Authorization'), $matches)
        ) {
            $token = $matches['token'];
        } elseif (!empty($request->getQueryParams()['token'])) {
            $token = $request->getQueryParams()['token'];
        } else {
            return $delegate->process($request);
        }

        if (!$this->autenticacao->setToken($token)->isValid()) {
            return $delegate->process($request);
        }

        $request = $request->withAttribute('user', $this->autenticacao->getUser());

        return $delegate->process($request);
    }
}