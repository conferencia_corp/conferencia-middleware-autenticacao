# Middleware Autenticacao #


A Simple middleware that uses the [ConferenciaCorp/Autenticacao](https://bitbucket.org/conferencia_corp/conferenciacorp-autenticacao) to authenticate 

## Installation

```
#!shell

composer require conferenciacorp/middleware-autenticacao
```